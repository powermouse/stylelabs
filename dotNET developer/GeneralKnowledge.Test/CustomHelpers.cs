﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data;

namespace GeneralKnowledge.Test.App
{
    public static class CustomHelpers
    {
        public static string ResiseImageFromURL(string imageUrl, int maxWidth, int maxHeight, bool isSaveImage, string destination = "")
        {
            string ImageName = String.Empty;
            byte[] imageBytes;
            destination = (destination.EndsWith(@"\")) ? destination : destination + @"\";

            string[] name = imageUrl.Split('?');
            ImageName = name[0].Substring(name[0].LastIndexOf("/", StringComparison.Ordinal) + 1);

            HttpWebRequest imageRequest = (HttpWebRequest)WebRequest.Create(imageUrl);
            WebResponse imageResponse = imageRequest.GetResponse();
            Stream responseStream = imageResponse.GetResponseStream();

            using (BinaryReader br = new BinaryReader(responseStream))
            {
                imageBytes = br.ReadBytes(500000);
                br.Close();
            }
            responseStream.Close();
            imageResponse.Close();

            using (MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                using (Image img = Image.FromStream(ms))
                {
                    int newWidth = 0; int newHeight = 0;
                    if (maxWidth != 0 && maxHeight != 0)
                    {
                        var ratioX = (double)maxWidth / img.Width;
                        var ratioY = (double)maxHeight / img.Height;
                        var ratio = Math.Min(ratioX, ratioY);
                        newWidth = (int)(img.Width * ratio);
                        newHeight = (int)(img.Height * ratio);
                        ImageName = "thimb_" + ImageName;
                    }
                    else
                    {
                        newWidth = img.Width;
                        newHeight = img.Height;
                    }
                    using (Bitmap b = new Bitmap(img, new Size(newWidth, newHeight)))
                    {
                        using (MemoryStream ms2 = new MemoryStream())
                        {
                            b.Save(ms2, System.Drawing.Imaging.ImageFormat.Jpeg);
                            imageBytes = ms2.ToArray();

                        }
                    }
                }
            }
            //save image _resized
            if (isSaveImage)
            {
                FileStream fs_resized = new FileStream(destination + "_" + ImageName, FileMode.Create);
                BinaryWriter bw_resized = new BinaryWriter(fs_resized);
                try
                {
                    bw_resized.Write(imageBytes);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    fs_resized.Close();
                    bw_resized.Close();
                }
            }
            return ImageName;
        }

        public static DataTable CSVtoDS(string filePath, string stringData, bool isFirstLineHeader)
        {
            DataTable dt = new DataTable();
            int c = 0;
            StreamReader sr = null;
            if (string.IsNullOrEmpty(filePath))
            {
                byte[] byteArray = Encoding.ASCII.GetBytes(stringData);
                sr = new StreamReader(new MemoryStream(byteArray));
            }
            else
                File.OpenText(filePath);

            using (TextReader tr = sr)
            {
                string strRow = string.Empty;
                string[] arrColumns = null;
                while ((strRow = tr.ReadLine()) != null)
                {
                    arrColumns = strRow.Split('\t');
                    //set up columns
                    if (c == 0)
                        if (dt.Columns.Count != arrColumns[0].Split(',').Count())
                        {
                            foreach (string el in arrColumns[0].Split(','))
                            {
                                if (isFirstLineHeader)
                                {
                                    dt.Columns.Add(new DataColumn(el));
                                }
                                else
                                {
                                    dt.Columns.Add(new DataColumn());
                                };
                            }
                        }
                    if (c > 0 || !isFirstLineHeader)
                    {
                        DataRow dr = dt.NewRow();
                        dr.ItemArray = SplitCSVString(arrColumns[0]);
                        dt.Rows.Add(dr);
                    }
                    c++;
                }
                tr.Close();
            }
            return dt;
        }

        public static string[] SplitCSVString(string input)
        {
            Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);
            List<string> list = new List<string>();
            string curr = null;
            foreach (Match match in csvSplit.Matches(input))
            {
                curr = match.Value;
                if (0 == curr.Length)
                {
                    list.Add("");
                }

                list.Add(curr.TrimStart(',')
                //remove wrapping quotes
                .TrimStart('\"')
                .TrimEnd('\"'));
            }

            return list.ToArray<string>();
        }
    }
   
}

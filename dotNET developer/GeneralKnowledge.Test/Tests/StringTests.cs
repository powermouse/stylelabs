﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO:
            // Complete the methods below

            AnagramTest();
            Console.WriteLine();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            Console.WriteLine("AnagramTest");
            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
            Console.WriteLine();
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";

            // TODO:
            // Write an algoritm that gets the unique characters of the word below 
            // and counts the number of occurences for each character found

            //Option 1
            Dictionary<string, int> d = word.GroupBy(x => x).Select(x => new { letter = x.First().ToString(), counter = x.Count() }).ToDictionary(x => x.letter, x => x.counter);
            //Option 2
            List<Tuple<string, int>> t = word.GroupBy(x => x).Select(x => new { letter = x.First().ToString(), counter = x.Count() }).Select(x => Tuple.Create(x.letter, x.counter)).ToList();

            Console.WriteLine("GetUniqueCharsAndCount");
            t.ForEach(c => Console.WriteLine(c.Item1 + " - " + c.Item2));
            Console.WriteLine();
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO: 
            // Write logic to determine whether a is an anagram of b

            return a.All(ch => b.ToList<char>().Remove(ch));
        }
    }
}

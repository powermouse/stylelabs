﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            var xDoc = XDocument.Parse(xml);
            xDoc.Descendants("param").Select(p => new
            {
                name = p.Attribute("name").Value,
                value = Convert.ToDecimal(p.Value)
            })
                        .GroupBy(g => g.name, g => g.value)
                        .Select(n => new { name = n.Key, LOW = n.Min(), AVG = decimal.Round(n.Average(), 2, MidpointRounding.AwayFromZero), MAX = n.Max() })
                        .ToList()
                        //results to screen
                        .ForEach(x => Console.WriteLine(String.Format("{0}\t: {1} \t {2} \t {3}",  
                                (x.name.Length < 8) 
                                    ? x.name + ("").PadRight(8, ' ')
                                    : x.name

                                , x.LOW, x.AVG, x.MAX)));

            Console.WriteLine();



        }
    }
}

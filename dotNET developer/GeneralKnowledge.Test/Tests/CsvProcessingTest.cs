﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;
            Console.WriteLine("reading data ****************");
            DataTable dt = CustomHelpers.CSVtoDS("", csvFile, true);
            Console.WriteLine("saving data ****************");
            
            //Parallel.ForEach(dt.AsEnumerable().AsParallel().WithDegreeOfParallelism(Math.Min(Environment.ProcessorCount / 2 * 10, 64)),
            //new ParallelOptions { MaxDegreeOfParallelism = 8 },
            //dr =>
            foreach (DataRow dr in dt.Rows)
            {
                using (var DataContext = new AssetsContextEF())
                {
                    Country _country = new Country() { Name = dr["country"].ToString() };
                    DataContext.Countries.AddOrUpdate(c => c.Name, _country);
                    DataContext.SaveChanges();

                    MimeType _mimeType = new MimeType() { Type = dr["mime_type"].ToString() };
                    DataContext.Mime_Types.AddOrUpdate(c => c.Type, _mimeType);
                    DataContext.SaveChanges();

                    Asset asset = new Asset()
                    {
                        AssetGUID = Guid.Parse(dr["asset id"].ToString()),
                        File_Name = dr["file_name"].ToString(),
                        Created_By = dr["created_by"].ToString(),
                        Email = dr["email"].ToString(),
                        Description = dr["description"].ToString(),
                        country = DataContext.Countries.Where(c => c.Name == _country.Name).FirstOrDefault(),
                        Mime_Type = DataContext.Mime_Types.Where(m => m.Type == _mimeType.Type).FirstOrDefault()
                    };
                    DataContext.Assets.AddOrUpdate(a => a.AssetGUID, asset);
                    DataContext.SaveChanges();
                }

            }


        }
    }
}

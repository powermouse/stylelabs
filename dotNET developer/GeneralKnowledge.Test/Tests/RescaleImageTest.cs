﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)

            string ImageURL = "https://pbs.twimg.com/media/Crq-DNuXgAA7bso.jpg";
            CustomHelpers.ResiseImageFromURL(ImageURL, 0, 0, true, @"D:\1\");
            CustomHelpers.ResiseImageFromURL(ImageURL, 150, 150, true, @"D:\1\");
        }
    }
}

namespace GeneralKnowledge.Test.App
{
    using GeneralKnowledge.Test.App.Tests;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class AssetsContextEF : DbContext
    {
        // Your context has been configured to use a 'Model1' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'GeneralKnowledge.Test.App.Model1' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Model1' 
        // connection string in the application configuration file.
        public AssetsContextEF()
            : base("name=AssetsContextEF")
        {
        }


        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //Mapping for the Customer Table
        //    modelBuilder.Entity<Asset>().HasKey(a => a.AssetID);
        //    modelBuilder.Entity<Country>().Property(a => a.CountryID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        //    modelBuilder.Entity<MimeType>().Property(c => c.MimeTypeID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        //    base.OnModelCreating(modelBuilder);
        //}

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<MimeType> Mime_Types { get; set; }
        public virtual DbSet<Country> Countries { get; set; }

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    [Serializable]
    public partial class Asset
    {
        [Key]
        public int AssetID { get; set; }
        public Guid AssetGUID { get; set; }
        [MaxLength(50)]
        public string File_Name { get; set; }
        [MaxLength(50)]
        public string Created_By { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        public Country country { get; set; }

        public MimeType Mime_Type { get; set; }

    }

    [Serializable]
    public partial class Country
    {
        [Key]
        public int CountryID { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }

        public Country() { }

    }

    [Serializable]
    public partial class MimeType
    {
        [Key]
        public int MimeTypeID { get; set; }

        [MaxLength(200)]
        public string Type { get; set; }

        public MimeType() { }
    }
}
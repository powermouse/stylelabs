﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebExperience.Test;
using WebExperience.Test.Models;

namespace WebExperience.Test.Controllers
{
    public class AssetsController : Controller
    {
        private WebExperienceEntities db = new WebExperienceEntities();

        // GET: Assets
        public ActionResult Index()
        {
            var assets = db.Assets.Include(a => a.Country).Include(a => a.MimeType);
            return View(assets.ToList());
        }

        // GET: Assets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if (asset == null)
            {
                return HttpNotFound();
            }
            return View(asset);
        }

        // GET: Assets/Create
        public ActionResult Create()
        {
            ViewBag.country_CountryID = new SelectList(db.Countries, "CountryID", "Name");
            ViewBag.Mime_Type_MimeTypeID = new SelectList(db.MimeTypes, "MimeTypeID", "Type");
            return View();
        }

        // POST: Assets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AssetID,AssetGUID,File_Name,Created_By,Email,Description,country_CountryID,Mime_Type_MimeTypeID")] Asset asset)
        {
            if (ModelState.IsValid)
            {
                db.Assets.Add(asset);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.country_CountryID = new SelectList(db.Countries, "CountryID", "Name", asset.country_CountryID);
            ViewBag.Mime_Type_MimeTypeID = new SelectList(db.MimeTypes, "MimeTypeID", "Type", asset.Mime_Type_MimeTypeID);
            return View(asset);
        }

        // GET: Assets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if (asset == null)
            {
                return HttpNotFound();
            }
            ViewBag.country_CountryID = new SelectList(db.Countries, "CountryID", "Name", asset.country_CountryID);
            ViewBag.Mime_Type_MimeTypeID = new SelectList(db.MimeTypes, "MimeTypeID", "Type", asset.Mime_Type_MimeTypeID);
            return View(asset);
        }

        // POST: Assets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AssetID,AssetGUID,File_Name,Created_By,Email,Description,country_CountryID,Mime_Type_MimeTypeID")] Asset asset)
        {
            if (ModelState.IsValid)
            {
                db.Entry(asset).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.country_CountryID = new SelectList(db.Countries, "CountryID", "Name", asset.country_CountryID);
            ViewBag.Mime_Type_MimeTypeID = new SelectList(db.MimeTypes, "MimeTypeID", "Type", asset.Mime_Type_MimeTypeID);
            return View(asset);
        }

        // GET: Assets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if (asset == null)
            {
                return HttpNotFound();
            }
            return View(asset);
        }

        // POST: Assets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Asset asset = db.Assets.Find(id);
            db.Assets.Remove(asset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
